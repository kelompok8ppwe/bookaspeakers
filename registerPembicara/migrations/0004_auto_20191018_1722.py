# Generated by Django 2.2.5 on 2019-10-18 10:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registerPembicara', '0003_auto_20191018_0009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pembicara',
            name='fee',
            field=models.BigIntegerField(),
        ),
    ]
