from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import forms

# Create your views here.
# def register(request):
#     form = forms.RegisterForm()
#     return render(request, 'register.html', {'form':form})

def register(request):
    if request.method == "POST":
        form = forms.RegisterForm(request.POST, request.FILES)
        if form.is_valid(): 
            form.save()
            return redirect('register:confirm')
    else:
        form = forms.RegisterForm()
    return render(request, 'register.html', {'form':form})

def confirm(request):
    return render(request, 'confirm.html')
