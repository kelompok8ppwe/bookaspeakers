from django.test import TestCase, Client
from .forms import RegisterForm

class registerPembicaraTest(TestCase):
    def test_urlregister(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)
    
    def test_namaweb(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn("BookaSpeaker", content)
    
    def test_ada_submit(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<input", content)
        self.assertIn ("Register", content)
        
    def test_form(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<form", content)
    
    def test_fill_booking_form(self):
        form_data = {
            'first_name': 'jonathan',
            'last_name': 'Chandra',
            'position' : 'CEO',
            'email': 'Jonathan@yahoo.com',
            'gender': 'male',
            'category': 'IT',
            'city': 'Depok',
            'fee': '200000',
            'birth_date': '02/06/2000',    
            'phone': '087887537138',
            'desc': 'Ini test',
        }
        form = RegisterForm(data = form_data)
        self.assertTrue(form.is_valid())
# Create your tests here.
