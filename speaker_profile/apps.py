from django.apps import AppConfig


class SpeakerProfileConfig(AppConfig):
    name = 'speaker_profile'
