from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

app_name = 'speaker_profile'

urlpatterns = [
    path('<id>', views.profile, name='profile'),
    path('<id>/check_availability/', views.check_availability, name='check_availability'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)