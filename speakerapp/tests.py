from django.test import TestCase, Client

class homepageTest(TestCase):
    def test_URL(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)
    
    def test_namaweb(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn("BookaSpeaker", content)
    
    def test_topspeaker(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn("Top Speakers", content)
    
    def test_button(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content)
        self.assertIn("Join as Speaker", content)

# Create your tests here.
