from django.shortcuts import render
from registerPembicara.models import Pembicara

# Create your views here.
def search(request):
    data = Pembicara.objects.all()
    if request.method == "GET":
        status = request.GET.get("search")
        if status == "Descending":
            data = Pembicara.objects.all().order_by("-fee")
        else:
            data = Pembicara.objects.all().order_by("fee")

    return render(request, "search.html", {
        "data_pembicara" : data,
    })

def search_all(request):
    data= Pembicara.objects.all()
    return render(request, 'search.html', {'data_pembicara':data})

def search_bycategory(request, category):
    if request.method == "GET":
        category = Pembicara.objects.all().filter(category = category)
    return render(request, "search.html", {"data_pembicara":category})

def search_bynama(request):
    value = request.GET.get('cari')   
    print(value) 
    firstname = Pembicara.objects.filter(first_name__icontains=value)
    lastname = Pembicara.objects.filter(last_name__icontains=value)
    kota = Pembicara.objects.filter(city__icontains=value)
    hasil = firstname | lastname | kota
    return render(request, "search.html", {"data_pembicara":firstname, "value":value})