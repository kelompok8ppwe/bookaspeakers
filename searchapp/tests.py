from django.test import TestCase, Client
from registerPembicara.models import Pembicara

# Create your tests here.
class SearchTest(TestCase):
    def test_url_exists(self):
        c = Client()
        response = c.get('/search/')
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        c = Client()
        response = c.get('/search/')
        self.assertTemplateUsed(response, 'search.html')

    def createPembicara(self, nama_depan="namaku", nama_belakang="namaku", posisi="bos", eemail="abc@d.com", jenis="laki", kota='Jakarta', harga="200000", tanggal_lahir="1999-10-10", notelp='081234567890', keterangan="oke"):
        return Pembicara.objects.create(first_name=nama_depan, last_name=nama_belakang, position=posisi, email=eemail, gender=jenis, city=kota, fee=harga, birth_date=tanggal_lahir, phone=notelp, desc=keterangan)

    def test_speaker_created(self):
        counter0 = Pembicara.objects.count()
        c = self.createPembicara()
        counter1 = Pembicara.objects.count()
        self.assertEqual(counter0 + 1, counter1)  
        self.assertTrue(isinstance(c, Pembicara))

    def test_page_contains_correct_html(self):
        c = Client()
        response = c.get('/search/')
        self.assertContains(response, '<option value="Descending">Fee (Highest to Lowest)</option>')